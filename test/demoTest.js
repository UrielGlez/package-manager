const assert = require('assert');
const sumar = require('../app');
//se ejecuta solo poniendo el comando mocha o npm run test
//afirmaciones

describe('Probar suma de numeros', () => {
    //definir casos de prueba
    //afirmar que cinco mas cinco es diez
    it('cinco mas cinco es diez', () => {
        assert.equal(10, sumar(5, 5));
    });
    //afirmar que cinco mas cinco no son cincuenta y cinco
    it('cinco mas cinco no son cincuenta y cinco', () => {
        assert.notEqual('55', sumar(5, 5));
    });

}); //agrupa la suit de pruebas







